#!/bin/bash

# This comes from
#
# https://medium.com/@kanrangsan/how-to-automatically-resize-virtual-box-disk-with-vagrant-9f0f48aa46b3

# The goal is to assume that the specified argument, the root partition,
# is a logical volume pointed to by /dev/centos/root

root_partition=$1
echo "centos_resize_root.sh..."
echo "root_partition: $root_partition"

disk=`echo $root_partition | sed 's/[0-9]\+$//'`
echo "disk: $disk"

sudo parted $disk resizepart 2 100%
sudo pvresize $root_partition
sudo lvextend -l +100%FREE /dev/centos/root
sudo xfs_growfs /dev/centos/root

