# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

require 'vagrant-aws'
require 'yaml'

CONF = YAML.load_file('config.yml')

#-------------------------------------------------------
#
#  This assumes that machines are provisioned with / on LVM, and
#  has specific, different partition names for vb and aws providers
#-------------------------------------------------------

Vagrant.configure("2") do |config|

    # Figure out the provider (https://github.com/mitchellh/vagrant-aws/issues/349)
    # We assume that if it's not aws (which would be specified on command line)
    # that it's virtualbox.  The "provider" variable is used further down
    # to specify name of root device
    provider_is_aws = (!ARGV.nil? && ARGV.join('').include?('provider=aws'))
    if provider_is_aws
        provider = "aws"
    else
        provider = "virtualbox"
    end
    puts "Provider: #{provider}"

    # This is for VirtualBox, it gets overridden for AWS
    config.vm.box = "bento/centos-7.7"
    config.vm.box_version = "201910.20.1"

    # Need vagrant-disksize plugin for this to work
    #     vagrant plugin install vagrant-disksize
    # https://github.com/sprotheroe/vagrant-disksize 
    # Note this needs to be done in "this" section.  It won't
    # work in the vb section
    config.disksize.size = CONF['vbox_disksize']

    # VirtualBox provider
    config.vm.provider "virtualbox" do |vb, override|
        # Customize the amount of memory on the VM:
        vb.name = CONF['vb_boxname']
        vb.memory = CONF['memory_mbytes']
        vb.cpus = CONF['cpus']

    end

    # AWS provider - note that this is an override
    config.vm.provider "aws" do |aws, override|

        # Override the default box (and version) specified above for VirtualBox 
        override.vm.box = CONF["DUMMY_AWS_CONFIG_BOX"]
        override.vm.box_version = ""

        # Get AWS access key id and secret access key
        # By default it will look in the environment variables and, if
        # those aren't defined, in $HOME/.aws/, default profile.  Alternatives
        # can be specified, and I have the profile specified here
        aws.aws_profile = CONF['aws_credentials_profile']

        # Specify SSH keypair to use
        aws.keypair_name = CONF['aws_keypair_name']

        # Specify region, AMI ID, and security group(s), subnet_id
        aws.region = CONF['aws_region']

        aws.ami = CONF['aws_ami']

        aws.instance_type = CONF['aws_instance_type']

        aws.security_groups = CONF['aws_security_groups']

        aws.subnet_id = CONF['aws_subnet_id']

        # I don't remember specifically what this one is for
        aws.associate_public_ip = true

        aws.terminate_on_shutdown = true

        # EBS volume size
        aws.block_device_mapping = [ 
            {
                'DeviceName' => '/dev/sda1',
                'Ebs.VolumeSize' => CONF['ebs_volume_size'] 
            } 
        ]

        # EC2 instance name tag
        aws.tags = {
            'Name' => CONF['aws_boxname']
        }


        # Specify username and private key path
        override.ssh.username = CONF['aws_ssh_username']
        override.ssh.private_key_path = CONF['aws_ssh_private_key_path']
  
        # Corrects what was once an AWS bug.  Not sure if still needed.
        # https://github.com/mitchellh/vagrant/issues/5401
        override.nfs.functional = false
    end


    # This assumes / is on LVM in CentOS and, right now, the partition needs
    # to be specified before calling
    # The "provider" variable is computed at beginning of file
    if provider == "virtualbox"
        root_partition = "/dev/sda2"
    elsif provider == "aws"
        root_partition = "/dev/xvda2"
    else
        puts "provider not supported: #{provider}"
    end

    config.vm.provision :shell, :path => "centos_resize_root.sh", :args => [root_partition]

    # Add in chef provisioning
    config.vm.provision :chef_solo do |chef|

        # This programmatically accepts license terms
        chef.custom_config_path = "CustomConfiguration.chef"

        chef.add_recipe "ctbto_centos_7.7"
    end
end  
