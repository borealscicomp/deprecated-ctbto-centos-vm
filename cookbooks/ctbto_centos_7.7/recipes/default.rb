#
# Cookbook:: ctbto_centos_7.7
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

#######  BEGIN TEMPORARY BLOCK COMMENT
###=begin
# This is necessary for many of the packages that follow
package 'epel-release'

# Package list comes from https://github.com/ctbtosa/atmdocs/wiki/Baseline-EL7
package 'ElectricFence'
package 'GMT'
package 'GMT-common'
package 'GMT-devel'
package 'GMT-doc'
package 'ImageMagick'
package 'PackageKit-gtk3-module'
package 'PyGreSQL'

package 'a2ps'
package 'apr'
package 'apr-devel'
package 'apr-util'
package 'apr-util-devel'
package 'apr-util-ldap'
package 'aspell'
package 'aspell-en'
package 'atlas'
package 'atlas-devel'
package 'atlas-sse3'
package 'autoconf'
package 'automake'

package 'bison'
package 'blas'
package 'blas-devel'
package 'boost-devel'
package 'boost-filesystem'
package 'boost-iostreams'
package 'boost-program-options'
package 'boost-regex'
package 'boost-signals'
package 'boost-system'
package 'boost-thread'
package 'byacc'
package 'bzip2-devel'

package 'cairo-devel'
package 'check'
package 'check-devel'
package 'cmake'
package 'compat-libcap1'
package 'cppunit'
package 'cppunit-devel'
package 'cscope'
package 'ctags'

package 'diffstat'
package 'discount'
package 'dos2unix'
package 'doxygen'

package 'eccodes-devel'
package 'eccodes-doc'
package 'emacs'
package 'environment-modules'
package 'expat-devel'
package 'expect'

package 'fftw'
package 'fftw-devel'
package 'fftw-static'
package 'fontconfig-devel'
package 'freetype-devel'

package 'gcc-c++'
package 'gcc-gfortran'
package 'gdal-python'
package 'gdb'
package 'gedit'
package 'geos'
package 'geos-devel'
package 'ghostscript'
package 'gimp'
package 'git'
package 'git-annex'
package 'git-gui'
package 'git-lfs'
package 'gitk'
package 'glibc'
package 'glibc-common'
package 'glibc-devel'
package 'glibc-headers'
package 'glibc-utils'
package 'graphviz'
#package 'grib_api'
#package 'grib_api-devel'

#  This fails provisioning, apparently because eccodes is already installed
#package 'grib_api-static'

package 'gsl'
package 'gsl-devel'
package 'gtk-recordmydesktop'

package 'hdf5'
package 'hdf5-devel'
package 'html2ps'

package 'indent'
package 'intltool'

package 'jasper'
package 'jasper-devel'
package 'jasper-libs'
package 'java-1.6.0-openjdk'
package 'java-1.6.0-openjdk-devel'

package 'kdegraphics'
package 'ksh'

package 'lapack'
package 'lapack-devel'
package 'libXaw'
package 'libXaw-devel'
package 'libXext'
package 'libXext-devel'
package 'libcanberra-gtk2'
package 'libcurl-devel'
package 'libmng'
package 'libpng-devel'
package 'libtool'
package 'libxml2-devel'
package 'libxslt-devel'
package 'log4cplus'
package 'log4cplus-devel'
package 'log4cpp'
package 'log4cpp-devel'

package 'make'
package 'matio'
package 'matio-devel'
package 'mesa-libGLw'
package 'mesa-libGLw-devel'
package 'myrepos'

package 'ncl'
package 'ncl-common'
package 'ncl-devel'
package 'ncl-examples'
package 'nedit'
package 'netcdf'
package 'netcdf-devel'
package 'netcdf-static'
package 'netcdf4-python'
package 'netcdf-fortran-devel'
package 'numpy'
package 'numpy-f2py'

package 'openldap-devel'
package 'openmpi-devel'
package 'openssl'
package 'openssl-devel'

package 'pandoc'
package 'patchutils'
package 'perl'
package 'perl-Carp'
package 'perl-Class-Inspector'
package 'perl-Config-General'
package 'perl-Crypt-SSLeay'
package 'perl-DBD-Pg'
package 'perl-DBD-SQLite'
package 'perl-DBI'
package 'perl-DateTime'
package 'perl-Digest-SHA'
package 'perl-Email-Address'
package 'perl-Exception-Base'
package 'perl-Exception-Class'
package 'perl-Exception-Class-TryCatch'
package 'perl-Exporter'
package 'perl-File-HomeDir'
package 'perl-File-Path'
package 'perl-File-Temp'
package 'perl-GD'
package 'perl-GDGraph'
package 'perl-Getopt-Long'
package 'perl-HTML-Format'
package 'perl-IO-Compress'
package 'perl-IO-Socket-INET6'
package 'perl-IO-Zlib'
package 'perl-IPC-Run'
package 'perl-JSON'
package 'perl-LWP-Protocol-https'
package 'perl-Log-Dispatch'
package 'perl-Mail-IMAPClient'
package 'perl-Module-ScanDeps'
package 'perl-Net-HTTP'
package 'perl-Net-SFTP-Foreign'
package 'perl-Number-Bytes-Human'
package 'perl-Object-InsideOut'
package 'perl-PathTools'
package 'perl-Pod-Usage'
package 'perl-SOAP-Lite'
package 'perl-Scalar-List-Utils'
package 'perl-Spreadsheet-ParseExcel'
package 'perl-Template-Toolkit'
package 'perl-Term-ProgressBar'
package 'perl-TermReadKey'
package 'perl-Tie-IxHash'
package 'perl-Time-HiRes'
package 'perl-Time-Local'
package 'perl-Tk'
package 'perl-URI'
package 'perl-libwww-perl'
package 'pixman-devel'
package 'poppler-utils'
package 'popt-static'
package 'postgresql'
package 'postgresql-devel'
package 'postgresql-libs'
package 'postgresql-odbc'
package 'pyodbc'
package 'python'
package 'python-basemap'
package 'python-lxml'
package 'python-matplotlib'
package 'python-psycopg2'
package 'python-virtualenv'
package 'python2-azure-storage'
package 'python36-numpy'
package 'python36-pytest'
package 'python36-wheel'
package 'python36-sip'
package 'python36-sip-devel'

package 'qt'
package 'qt-devel'
package 'qt-mysql'
package 'qt-odbc'
package 'qt-postgresql'
package 'qt-x11'
package 'qt5-qtimageformats'
package 'quazip'
package 'quazip-devel'

package 'readline'
package 'readline-devel'
package 'root'
package 'root-core'
package 'root-fftw'
package 'root-foam'
package 'root-fumili'
package 'root-gdml'
package 'root-genetic'
package 'root-genvector'
package 'root-geom'
package 'root-graf'
package 'root-graf-asimage'
package 'root-graf-fitsio'
package 'root-graf-gpad'
package 'root-graf-gviz'
package 'root-graf-postscript'
#package 'root-graf-qt'
package 'root-graf-x11'
package 'root-graf3d'
package 'root-graf3d-eve'
package 'root-graf3d-gl'
package 'root-graf3d-gviz3d'
package 'root-graf3d-x3d'
package 'root-gui'
package 'root-gui-fitpanel'
package 'root-gui-ged'
#package 'root-gui-qt'
package 'root-gui-recorder'
package 'root-hbook'
package 'root-hist'
package 'root-hist-factory'
package 'root-hist-painter'
package 'root-html'
package 'root-icons'
package 'root-io'
package 'root-io-dcache'
package 'root-io-gfal'
#package 'root-io-rfio'
package 'root-io-sql'
package 'root-io-xml'
package 'root-mathcore'
package 'root-mathmore'
package 'root-matrix'
package 'root-memstat'
package 'root-minuit'
package 'root-minuit2'
package 'root-mlp'
package 'root-physics'
package 'root-proof'
#package 'root-proof-pq2'
package 'root-proof-sessionviewer'
#package 'root-proofd'
package 'root-quadp'
package 'root-roofit'
#package 'root-rootd'
package 'root-smatrix'
package 'root-spectrum'
package 'root-spectrum-painter'
package 'root-splot'
#package 'root-table'
package 'root-tmva'
package 'root-tree'
package 'root-tree-player'
package 'root-tree-viewer'
package 'root-tutorial'
package 'root-unuran'
package 'root-xproof'

package 'splint'
package 'sqlite-devel'
package 'swig'

package 'tcl'
package 'tcl-devel'
package 'texlive-latex'
package 'thunderbird'
package 'tigervnc-server'
package 'time'
package 'tk'
package 'tk-devel'
package 'tkcvs'
package 'tkinter'
package 'tmpwatch'

package 'valgrind'

package 'x2goclient'
package 'x2goserver'
package 'xerces-c'
package 'xmlto'
package 'xorg-x11-apps'
package 'xorg-x11-fonts-75dpi'
package 'xorg-x11-fonts-ISO8859-2-75dpi'
package 'xorg-x11-server-Xvfb'
package 'xterm'

package 'zlib'
package 'zlib-devel'
###=end
#######  END TEMPORARY BLOCK COMMENT






user 'ctbtuser' do
    home '/home/ctbtuser'
    shell '/bin/bash'
    manage_home true
    action :create
end

bash 'set_ctbtuser_password' do
    code <<-EOH
        echo -e "ctbtuser\nctbtuser" | passwd ctbtuser
    EOH
end

# Allow for password login
bash 'set_sshd_passwd_auth' do
    code <<-EOH
        sed "/PasswordAuthentication/ c\PasswordAuthentication yes" -i /etc/ssh/sshd_config
        systemctl restart sshd.service
    EOH
end


# Set timezone to UTC
bash 'set_timezone_to_utc' do
    code <<-EOH
        timedatectl set-timezone UTC
    EOH
end





file "/home/ctbtuser/hello.txt" do
    content "Hello, CTBTO"
end

