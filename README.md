# ctbto-centos-vm

This is the Vagrant and Chef setup for a CentOS 7.7 VM for use in CTBTO 
activities.  It will provision for both VirtualBox (the default) or an
AWS EC2 instance.  This README currently assumes VirtualBox.

The provisioned packages are those listed (I think there may be a couple of
exceptions) at https://github.com/ctbtosa/atmdocs/wiki/Baseline-EL7

# Quick Start

Hopefully I've included all that a "first time" user needs, but it's possible
there are other prerequisite requirements that I've forgotten about (because
I already have them on my own machine)

* Insure you have a recent version of *Vagrant* installed.  I used version 2.2.3, but it shouldn't be very picky
* You'll need to install a couple of *vagrant* plugins.  Even though these guidelines do not include AWS provisioning, it "is" built in to the *Vagrantfile*, which means the plugin needs to be accessible
    * `$ vagrant plugin install vagrant-disksize`
    * `$ vagrant plugin install vagrant-aws`
* Clone this repo and go to the main directory
* Consider edits of *config.yml* especially date of the box
* Run `vagrant up` to create the VM and start provisioning the many packages



# Provisioning a new machine

Before doing this, look at *config.yml* and see if there is anything that
might need to be customized, especially the date on the new box:

```
vb_boxname: 'CTBTO_CentOS_7.7_Provisioned_2020-02-19'
aws_boxname: 'CTBTO_CentOS_7.7_Provisioned_2020-01-06'
```

or AWS instance specifications.


```
$ vagrant up 2>&1 | tee vb.log
Provider: virtualbox
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'bento/centos-7.7'...
.
.
.
==> default: [2019-11-27T03:50:36+00:00] INFO: Chef Infra Client Run complete in 983.881142491 seconds
==> default: 
==> default: Running handlers:
==> default: [2019-11-27T03:50:36+00:00] INFO: Running report handlers
==> default: Running handlers complete
==> default: [2019-11-27T03:50:36+00:00] INFO: Report handlers complete
==> default: Chef Infra Client finished, 201/297 resources updated in 16 minutes 25 seconds
```



# Access to the VM

As soon as you start provisioning, it should show up in the VirtualBox console.
Even while it's provisioning the packages, you can access the VM (you need to
do this from the same directory in which you ran `vagrant up`)

```
$ vagrant ssh
Provider: virtualbox

This system is built by the Bento project by Chef Software
More information can be found at https://github.com/chef/bento/README.md
[vagrant@localhost ~]$
```

You can also log in via direct ssh by first finding the port number

```
$ vagrant port
Provider: virtualbox
The forwarded ports for the machine are listed below. Please note that
these values may differ from values configured in the Vagrantfile if the
provider supports automatic port collision detection and resolution.

    22 (guest) => 2200 (host)
```

then

```
$ ssh -p 2200 vagrant@localhost
```

Password is same as userid, and you have `sudo` access through this account.


There is also a user account, *ctbtuser* (password same as username).  This
won't be accessible until the full provisioning has completed.


```
$ ssh -p 2200 ctbtuser@localhost
```

# Interesting files

Just in case you want to poke around - these are the key files 

* *Vagrantfile* - `vagrant up` looks here first
* *config.yml*
* *cookbooks/ctbto_centos_7.7/recipes/default.rb* - The Chef "recipe" for provisioning


# Starting an exsiting VirtualBox VM that may have been shut down

Rather than going through provisioning to start a VM that's been turned off, we can use the VirtualBox CLI to get info (for example, port number) on the VM and then start it in headless mode (if desired).

```
$ vboxmanage list vms
"openconnect-20181227" {4ad64fbe-ab54-4d04-8a8d-e82c9948a78b}
"CTBTO_CentOS_7.7_Provisioned_2019-11-11" {73f03e14-6b8d-49d0-9fd6-8c307ac793d0}

$ vboxmanage showvminfo "CTBTO_CentOS_7.7_Provisioned_2019-11-11"
.
.
.
NIC 1 Rule(0):   name = ssh, protocol = tcp, host ip = 127.0.0.1, host port = 2200, guest ip = , guest port = 22
.
.
.
```

then start the box running if it isn't already

```
$ vboxmanage startvm "CTBTO_CentOS_7.7_Provisioned_2019-11-11" --type headless
Waiting for VM "CTBTO_CentOS_7.7_Provisioned_2019-11-11" to power on...
VM "CTBTO_CentOS_7.7_Provisioned_2019-11-11" has been successfully started.
```

and then *ssh* in to correct port number (see the `host port` field in the `showvminfo` example above) using either the *vagrant* user for `sudo` access or *ctbtuser* for unprivileged access.




